<?php
	include_once 'inc/top.php';
         
?>
    <div class="container">

        <div class="starter-template">       
    <?php
        try {
            //avataan tietokantayhteys
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // Muodostetaan suoritettava sql-lause.
            $sql = 'SELECT * FROM kirjoitus ORDER BY paivays DESC';
            // Suoritetaan kysely tietokantaan.
            $kysely = $tietokanta->query($sql);
            
            if ($kysely){

                
                while ($tietue = $kysely->fetch()) {
                    print '<div class="blogidiv">';
                    $pvm = strtotime($tietue['paivays']);
                    $pvmmuoto = date("d.m.Y H:i", $pvm);
                    print '<p><strong>' . $tietue['otsikko'] . "</strong> " . $pvmmuoto . '</p>';                    
                    print '<p>' . $tietue['teksti'] . '</p>';
                    if (isset($_SESSION['kayttaja_id'])) {
                        //tässä yritin laittaa glyphiconia <span class="glyphicon glyphicon-trash"></span> + linkittää glyphiconien css-tiedoston top.php, mutta se sotki sivun asettelun
                        print '<p><a href="poista.php?id=' . $tietue['id']. '" onclick="return confirm(\'Jatketaanko?\');">Poista</a></p>';            
                    }
                    else {
                        print '<p style="font-size: 0.5em;">Kirjaudu sisään, jotta voit editoida postausta!</p>' ;
                    }
                    print '<hr noshade="no shade">' ;
                    print '</div>';
                }
            }
            else{
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
        } 
        catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
    ?>
      </div>
    </div>

<?php
	include_once 'inc/bottom.php';
?>


