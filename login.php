<?php
include_once 'inc/top.php';

$viesti = "";
//tietokantayhteys
$tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
$tietokanta->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

if ($_SERVER['REQUEST_METHOD'] ==='POST') {
    if ($tietokanta!=null) {
        try {
            //tunnus ja salasana otetaan lomakkeesta
            $tunnus = filter_input(INPUT_POST,'tunnus',FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST,'salasana', FILTER_SANITIZE_STRING));
            //sql-lause
            $sql = "SELECT * FROM kayttaja where tunnus='$tunnus' AND salasana='$salasana'";
            //sql-lause syötetään tietokantaan
            $kysely = $tietokanta->query($sql);          
            
            if ($kysely->rowCount()===1) {
                $tietue = $kysely->fetch();
                $_SESSION['login'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                //ohjataan index.php-sivulle
                header('Location: index.php');
            }
            else {
                $viesti = "Väärä tunnus tai salasana!";
            }                         
        } catch (PDOException $pdoex) {
                print "<p>Käyttäjän tietojen hakeminen epäonnistui." . $pdoex->getMessage() . '</p>';
        }
        //print $_SESSION['kayttaja_id'];
        print $viesti;
    }
}
?>
    <main role="main" class="container">
        <div class="starter-template">
            <form action=" " method="post">
                <label><b>Käyttäjätunnus</b></label></br>
                <input type="text" placeholder="Tunnuksesi" name="tunnus" required></br>
                <label><b>Salasana</b></label></br>
                <input type="password" placeholder="******" name="salasana" required></br>
                <button type="submit" style="margin:1em;">Kirjaudu</button></br>
                <input type="checkbox" checked="checked"> Muista
            </form> 
            <p></span></p>
        </div>
    </main>
<?php
	include_once 'inc/bottom.php';
?>
