<?php   

include_once 'inc/top.php';
try {

            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
   
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Suoritetaan kysely tietokantaan.
            $sql = 'SELECT * FROM kirjoitus';
            $kysely = $tietokanta->query($sql);
            
            if ($kysely) {
                while ($tietue = $kysely->fetch()) {

                    print '<p>' . $tietue['otsikko'] . '</p>';
                    print '<p>' . $tietue['teksti'] . '</p>';  
                }
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
?>
