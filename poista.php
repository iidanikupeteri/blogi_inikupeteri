<?php
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);

        try {
            // Avataan tietokantayhteys.
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Muodostetaan parametroitu sql-kysely tiedon poistamista varten.
            $kysely = $tietokanta->prepare("DELETE FROM kirjoitus WHERE id='$id'");


            // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
            if ($kysely->execute()) {
                print('<p>Blogitekstisi on poistettu!</p>');
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            print("<a href='index.php'>Etusivulle</a>");


        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
        ?>