<?php
include_once 'inc/top.php';

if ($_SERVER['REQUEST_METHOD']==='POST') {
            try {
                //avataan tietokantayhteys
                $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
                $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //muuttujat
                $kayttaja_id = 1;
                $otsikko = filter_input(INPUT_POST, 'otsikko',FILTER_SANITIZE_STRING);
                $teksti = filter_input(INPUT_POST, 'teksti',FILTER_SANITIZE_STRING);
                $paivays = "";
                //sql-kysely
                $kysely = $tietokanta->prepare("INSERT INTO kirjoitus(otsikko, teksti, kayttaja_id) VALUES ('$otsikko','$teksti','$kayttaja_id')");
               
                if ($kysely->execute()) {
                    print('<p>Teksti tallennettu!</p>');
                }
                else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
                print("<a href='index.php'>Etusivulle</a>");

            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
        }
        ?>
<body>
      <div class="starter-template">
          <h1 style="font-weight: bold;">Lisää kirjoitus</h1>
            <form action="" method="post" enctype="multipart/form-data" >
                <label for="otsikko"><strong>Otsikko</strong></label><br>
                <input type="text" name="otsikko" maxlength="50" id="otsikko" placeholder="Otsikko tähän" style="width:40%; height:1.5em; border-radius: 5%;" ><br>
                <label for="teksti" style="margin-top: 0.5em;"><strong>Kirjoitus</strong></label><br>
                <textarea name="teksti"  rows="10" maxlength="500" id="teksti" placeholder="Teksti tänne.." style="width:40%;height:150px; border-radius: 2%;"></textarea><br>
                <input type="submit" value="Tallenna" style="margin-right: 1em; margin-top:1em;"><input type="reset" value="Peruuta" style="margin-top:1em;">
            </form>

      </div>

<?php
	include_once 'inc/bottom.php';
?>
